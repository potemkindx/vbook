#ifndef ADDITIONALITEM_H
#define ADDITIONALITEM_H

#include <QObject>

class additionalItem : public QObject
{
    Q_OBJECT
public:    
    Q_PROPERTY(QString text    MEMBER text CONSTANT)
    Q_PROPERTY(QString image   MEMBER image CONSTANT)
    Q_PROPERTY(int     type    MEMBER type CONSTANT)

    explicit additionalItem(QObject *parent = 0);

    QString text;
    QString image;
    int type;
private:

};

#endif // ADDITIONALITEM_H
