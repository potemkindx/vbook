#ifndef VITEM_H
#define VITEM_H

#include <QObject>

class VItem : public QObject
{
    Q_OBJECT
public:
    explicit VItem(QObject *parent = 0);
    Q_PROPERTY(QString name READ name NOTIFY nameChanged)
    Q_PROPERTY(QString count READ count WRITE setCount NOTIFY countChanged)
    Q_PROPERTY(QString image READ image WRITE setImagePath NOTIFY imageChanged)
    Q_PROPERTY(bool isVisible MEMBER isVisible NOTIFY isVisibleChanged)

    QString name();
    QString count();
    QString image();

    void setName(QString);
    void setCount(QString);
    void setImagePath(QString);

signals:
    void nameChanged();
    void countChanged();
    void imageChanged();
    void isVisibleChanged();
public slots:
private:
    bool isVisible;
    QString m_name;
    QString m_count;
    QString m_image;
};


#endif // VITEM_H
