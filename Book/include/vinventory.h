#ifndef VINVENTORY_H
#define VINVENTORY_H

#include <QObject>
#include <QDomDocument>

class VInventory : public QObject
{
    Q_OBJECT
public:
    explicit VInventory(QString filename, QObject *parent = 0);

    Q_PROPERTY(bool isVisible MEMBER isVisible CONSTANT)
    Q_PROPERTY(QString text   MEMBER text CONSTANT)
    Q_PROPERTY(QList<QObject*> items MEMBER items CONSTANT)

    QList<QObject*> items;
private:
    QDomDocument doc;
    QDomElement root;
    QDomElement info;
    bool isVisible;
    QString text;
};

#endif // VINVENTORY_H
