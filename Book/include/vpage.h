#ifndef VPAGE_H
#define VPAGE_H

#include <QString>
#include <QObject>
class VPage {
public:
    explicit VPage(QString mSource);
    explicit VPage();

    QString mSource;
    QString Text;
    QList<QObject*> answers;

    QString body_image;
    QString head_image;
    QString bottom_image;
private:
    static QString Path;
    static QString textExtension;
    static QString answersExtension;
    static QString infoExtension;
};

#endif // VPAGE_H
