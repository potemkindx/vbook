#ifndef VANSWER_H
#define VANSWER_H

#include <QObject>
#include <QDomDocument>

class VAnswer : public QObject {
    Q_OBJECT
public:
    explicit VAnswer(QString filename, QObject *parent = 0);

    Q_PROPERTY(QString answerText READ answerText CONSTANT)
    Q_PROPERTY(bool isVisible MEMBER isVisible CONSTANT)
    Q_PROPERTY(bool isRandom MEMBER isRandom CONSTANT)

    QString answerText();
    bool isVisible;
public slots:
    QString nextPage();
    bool perfomActions();
protected:
    bool checkCondtion(const QDomNode & condition);
    void performAction(const QDomNode & action);
    bool processConditions(const QDomNode & type);
private:
    bool isRandom;
    QDomDocument doc;
    QDomElement root;
    QDomNode info;
    QDomNode visible;
    QDomNode actions;
    QDomNode type_conditions;
    QDomNode next_page;
};

#endif // VANSWER_H
