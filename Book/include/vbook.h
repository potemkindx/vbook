#ifndef VBOOK_H
#define VBOOK_H

#include <QObject>
#include <QList>
#include <QVector>
#include <QSettings>

#include "vpage.h"
#include "vitem.h"


class VBook : public QObject {
    Q_OBJECT
public:
    static void registerType();
public:
    explicit VBook(QObject *parent = 0);
    Q_PROPERTY(QString pageText READ pageText NOTIFY pageChanged)

    Q_PROPERTY(QList<QObject*> pageAnswers READ pageAnswers NOTIFY pageChanged)
    Q_PROPERTY(int ansSize READ answersSize NOTIFY pageChanged)

    Q_PROPERTY(QList<QObject*> inventory READ inventory NOTIFY bookInventoryChanged)
    Q_PROPERTY(int invSize MEMBER invSize NOTIFY bookInventoryChanged)

    Q_PROPERTY(QString body_image MEMBER body_image NOTIFY pageChanged)
    Q_PROPERTY(QString head_image MEMBER head_image NOTIFY pageChanged)
    Q_PROPERTY(QString bottom_image MEMBER bottom_image NOTIFY pageChanged)

    Q_PROPERTY(QList<QObject*> additional READ additional NOTIFY additionalChanged)
    Q_PROPERTY(int addSize MEMBER additionalSize NOTIFY additionalChanged)

    Q_INVOKABLE void nextPage(QString choice);
    Q_INVOKABLE void init();

    QString pageText();
    QList<QObject*> pageAnswers();
    int answersSize();

    QList<QObject*> additional();



    static QMap < QString, VItem *> items;
    static QSettings settings;
    QList<QObject*> inventory();
signals:
    void pageChanged();
    void bookInventoryChanged();
    void additionalChanged();
public slots:
    QList<QObject*> bookInventory(int i);
private:
    VPage *CurrentPage;
    QList<QObject *> inv;
    int invSize;

    QString body_image;
    QString head_image;
    QString bottom_image;

    QString body_image_book;
    QString head_image_book;
    QString bottom_image_book;

    int additionalSize;
};

#endif // VBOOK_H
