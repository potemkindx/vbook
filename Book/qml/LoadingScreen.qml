import QtQuick 2.5
import QtQuick.Controls 1.4

Rectangle // блокирование экрана до получения ответа сервера с обработкой запроса
{
    property bool is_blocked : true
    visible: is_blocked

    color: "#77ffffff"

    BusyIndicator
    {
        id: loadIndicatorBlocker
        anchors.centerIn: parent
        running: parent.is_blocked
    }
}

