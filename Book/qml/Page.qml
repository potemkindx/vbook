import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item
{
    property real footersHeight: mainWindow.height * 0.05
    Rectangle {
        id: mainText
        color: "transparent"

        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: answers.top

        Flickable {
            id: flickArea
             anchors.fill: parent
             contentWidth: parent.width;
             contentHeight: bodyBackgroundImage.height + headerBackgroundImage.height + bottomBackgroundImage.height
             flickableDirection: Flickable.VerticalFlick
             boundsBehavior: Flickable.StopAtBounds

             clip: true

             Image {
                 id: headerBackgroundImage
                 width: parent.width
                 height: footersHeight
                 fillMode : Image.Stretch
                 source: book.head_image
             }


             Image {
                 id: bodyBackgroundImage
                 anchors.top: headerBackgroundImage.bottom
                 height: (textArea.height > (mainText.height - 2*footersHeight) ) ? textArea.height : (mainText.height - 2*footersHeight)
                 width: parent.width
                 fillMode : Image.Stretch
                 source: book.body_image
             }

             Text
             {
                 id: textArea
                 anchors.top: bodyBackgroundImage.top
                 anchors.left: bodyBackgroundImage.left
                 anchors.right: bodyBackgroundImage.right
                 anchors.leftMargin: parent.width * 0.07;
                 anchors.rightMargin: parent.width * 0.07;
                 textFormat: Text.RichText
                 wrapMode: Text.WordWrap
                 font.family: "Verdana"

                 text: book.pageText
             }


             Image {
                 id: bottomBackgroundImage
                 anchors.top: bodyBackgroundImage.bottom
                 width: parent.width
                 height: footersHeight
                 fillMode : Image.Stretch
                 source: book.bottom_image
             }


             onMovementEnded: {

                 if(answers.height == 0 && atYEnd) {
                     answers.height = mainWindow.height/3;
                     contentY += mainWindow.height/3;
                 }
             }

        }
        MouseArea {
            visible: flickArea.atYEnd;
            anchors.fill: parent;
            onClicked: {
                if(answers.height == 0 && flickArea.atYEnd) {
                    answers.height = mainWindow.height/3;
                    flickArea.contentY += mainWindow.height/3;
                }
            }
        }
    }

    Rectangle {
        id: answers
        height: 0
        width: parent.width
        anchors.bottom: parent.bottom
        color: "#77607d8d"
        Behavior on height {
             NumberAnimation {
                 duration: 200
                 //This selects an easing curve to interpolate with, the default is Easing.Linear
                 easing.type: Easing.Linear
             }
         }
    }

    ScrollView
    {
        id: scrl_answers
        property int button_height : mainWindow.height / 12

        property int margin: (book.ansSize == 1) ? (3*answers.height) / 8  :
                             (book.ansSize == 2) ? (2*answers.height) / 12 :
                             (book.ansSize == 3) ? (1*answers.height) / 16 : 0

        property int spacing: (book.ansSize == 1) ? 0 : margin

        anchors.fill: answers
        anchors.topMargin: margin
        anchors.bottomMargin: margin

        ListView
        {   
            id:lvr
            anchors.fill: parent

            spacing : scrl_answers.spacing
            model: book.pageAnswers

            delegate: Button {
                text: modelData.answerText

                visible: modelData.isVisible
                enabled: modelData.isVisible

                width: lvr.width
                height: modelData.isVisible*scrl_answers.button_height

                onClicked: {
                    answers.height = 0
                    book.loadPage(modelData.nextPage());
                }


                style: ButtonStyle
                {
                    background: Rectangle {
                        radius: 1 / 2
                        color: "#607d8d"
                        Rectangle {
                            color: "#aac8d0"
                            anchors.bottom: parent.bottom
                            anchors.left: parent.left
                            anchors.leftMargin: (mainWindow.width * 0.07)
                            anchors.rightMargin: (mainWindow.width * 0.07)
                            anchors.right: parent.right
                            height: (parent.height / 10)
                            radius: 5
                        }
                    }
                    label: Row {
                        id: row
                        anchors.centerIn: parent
                        Text {
                            id: text
                            width: lvr.width
                            renderType: Text.NativeRendering
                            anchors.verticalCenter: parent.verticalCenter

                            horizontalAlignment: Text.AlignHCenter
                            text: control.text
                            wrapMode: Text.WordWrap
                        }
                    }
                }
            }
        }
    }
}
