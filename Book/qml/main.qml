import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.3
import QtMultimedia 5.5

import VBook 1.0

ApplicationWindow
{
    id: mainWindow
    objectName: "mainWindow"
    visible: true
    width: 480      // 480x800 must populated device resolution with mdpi
    height: 800
    color: "white"

    ExitDialog { id: exitDialog }

    VBook {
        id: book
        function loadPage(value) {
            stack.pushWindow("LoadingScreen");
            book.nextPage(value);
            stack.popWindow()
        }
    }

    MediaPlayer {
        id: player
        source: "http://www.dancemidisamples.com/samples/PSY-ACADEMY.mp3"
    }

    Item {
        anchors.fill: parent
        focus: true

        Keys.onEscapePressed: { stack.popWindow() }
        Keys.onBackPressed:   { stack.popWindow() }

        StaticButtons {
            id: buttonsBlock
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right

        }
        StackView
        {
            id: stack
            width: parent.width
            anchors.top: buttonsBlock.bottom
            anchors.bottom: parent.bottom
            initialItem: ({item: "qrc:/LoadingScreen.qml", destroyOnPop: true})

            property string strQmlWindow;
            function pushWindow(strQml) {
                if(strQmlWindow != strQml) {
                    strQmlWindow = strQml
                    console.log("Push window " + strQml)
                    stack.push({item: "qrc:/" + strQml + ".qml", destroyOnPop: true})
                }
            }

            function popWindow() {
                if(stack.depth > 1) {
                    strQmlWindow = "";
                    stack.pop()
                } else {
                    exitDialog.open()
                }

            }
        }
    }


    Component.onCompleted: {
        book.init();
        stack.clear();
        stack.pushWindow("Page");
    }
}

