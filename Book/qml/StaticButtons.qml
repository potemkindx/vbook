import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2
import QtMultimedia 5.5

Rectangle {
    height: 100
    width: parent.width
    property int buttons_count: 2 //может как-то автоматизировать?
    property int button_height: height;
    property int button_width: (width - 0.315 * width) / 2;

    Image {
        anchors.fill: parent
        sourceSize.height: parent.height
        sourceSize.width: parent.width
        source: "qrc:/menu/header.svg"
    }

    Row {
        height: 100
        width: parent.width

        InventoryButton {
            id: inventory
            width: button_width
            implicitHeight: button_height
        }

        AdditionalButton {
            id: settings_button
            implicitWidth: button_width
            implicitHeight: button_height
        }


        SettingsButton {
            id: button3
            width: parent.width * 0.315
            height: button_height
        }


    }

}
