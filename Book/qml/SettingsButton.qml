import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4


Item {
    id: audioPlayer

    //property alias text: innerText.text
    property string nosound : "https://cdn0.iconfinder.com/data/icons/shift-icons-glyphs/16/No_Sound-128.png"
    property string ssource : "https://cdn2.iconfinder.com/data/icons/flat-ui-icons-24-px/24/volume-24-128.png"
    signal clicked
    property bool actived_menu : false

    Button {
        anchors.fill: parent
        text: ""
        style: ButtonStyle {
            background: Rectangle {
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    source: actived_menu ? "qrc:/menu/setting-button.svg" : "qrc:/menu/setting-button-unchecked.svg"
                    sourceSize.height: parent.height*0.4
                    sourceSize.width: height
                    fillMode: Image.PreserveAspectFit
                    mipmap: true
                    autoTransform: true
                }
            }
        }
        onClicked: {
            actived_menu = true;
            stack.pushWindow("SettingsWindow");
        }
    }

}

