import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item {
    Image {
        id: menuHeader
        anchors.top:parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        height: parent.height / 3
        source: "qrc:/menu/menu.svg"
        Text {
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height*0.1;
            anchors.left: parent.left
            anchors.leftMargin: parent.width*0.1
            text: "Меню"
            font.family: "Verdana"
            font.pointSize: 40
            color: "White"
        }
    }





    Column {
        anchors.top: menuHeader.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom

        Rectangle {
            radius: 10.0
            width: parent.width;
            height:  parent.height*0.1
            Row {
               anchors.centerIn: parent
               height: parent.height*0.1
               anchors.left: parent.left
               anchors.leftMargin: parent.width * 0.1
               anchors.right: parent.right
               anchors.rightMargin: parent.width * 0.1

               Text {
                   font.pixelSize: 32
                   text: "Звук"
               }
               CheckBox {
                   onCheckedChanged: {
                       if(player.playbackState == player.PlayingState) {
                           player.stop();
                       } else  {
                           player.play()
                       }
                   }
               }
           }
        }


    }
}



