import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item {
    id: baseItem
    property var model: book.inventory;
    property int size:  book.invSize

    ComboBox {
        id: button2_1
        anchors.fill: parent

        model: baseItem.model
        visible: baseItem.size > 1

        onCurrentIndexChanged: {
            if(visible && currentIndex != -1 && model[currentIndex]) {
                stack.pushWindow("InventoryWindow");
                stack.currentItem.loadModel(model[currentIndex].items);
                currentIndex = -1;
            }
        }

        onModelChanged: {
            currentIndex = -1;
        }

        style:ComboBoxStyle {
            background: Rectangle {
                color: "transparent"
            }

            label: Item {
                   implicitWidth: textitem.implicitWidth + 20
                   baselineOffset: textitem.y + textitem.baselineOffset
                   Text {
                       id: textitem
                       anchors.centerIn: parent
                       text: "Button 2"
                       renderType: Text.NativeRendering
                       font.family: "Verdana"
                       color: "black"
                       elide: Text.ElideRight
                   }

               }
        }
    }

    Button
    {
        id: button2_2
        anchors.fill: parent

        visible: (baseItem.size === 1)

        text: "SButton2"
        onClicked: {
            stack.pushWindow("InventoryWindow");
            stack.currentItem.loadModel(model[currentIndex].items);
        }

        style: ButtonStyle
        {
            background: Rectangle {
                color: "#00ffffff"
                Image {
                    anchors.fill: parent
                    source: "qrc:/book_res/button1.jpg"
                }
            }
        }
    }
}

