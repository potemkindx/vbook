import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2


Item {
    function loadModel(model) {
        content.model = model;
    }

    Rectangle {
        id: dialogItem
        anchors.fill: parent
        color: "lightskyblue"

        ListView {
            id: content;
            anchors.fill: parent
            spacing: 10
            delegate: Rectangle {
                width: parent.width
                implicitHeight: button.height
                Button {
                    id:button
                    text: modelData.name
                    onClicked: (console.log())
                }
                Text {
                    id:count
                    height: parent.height
                    anchors.left: button.right
                    anchors.leftMargin: 10;
                    text: modelData.count
                }
                Image {
                    height: 20
                    width: 20
                    anchors.left: count.right
                    anchors.leftMargin: 10
                    source: modelData.image
                }
            }
        }
    }
}


