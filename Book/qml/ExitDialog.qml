import QtQuick 2.5
import QtQuick.Dialogs 1.2

MessageDialog
{
    id: m_dialog
    title: "Title"
    text: "Rly?"
    standardButtons: StandardButton.Yes | StandardButton.No
    onYes: Qt.quit()
}

