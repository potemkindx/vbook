import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

Item {
    function setType(iType) {
        type = iType.type
        console.log("type" + type)
    }

    property int type: 0;
    Image {
        id: dialogItem
        anchors.fill: parent
        source: "qrc:/book_res/button1.jpg"
        TextEdit {
            enabled: (type == 0)
            font.family: "Verdana"
            anchors.fill: parent
            anchors.topMargin: 20;
            anchors.leftMargin: 20;
            anchors.rightMargin: 20;
            anchors.bottomMargin: 20;
        }
    }
}

