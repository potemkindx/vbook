#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "vbook.h"

int main(int argc, char *argv[])
{

    QGuiApplication app(argc, argv);
    VBook::registerType();


    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

