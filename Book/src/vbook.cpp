#include "vbook.h"

#include <QQmlEngine>
#include <QtQml>
#include <QDir>

#include "vanswer.h"
#include "vinventory.h"
#include "globals.h"
#include "additionalitem.h"

QMap < QString, VItem*> VBook::items;
QSettings VBook::settings(QSettings::NativeFormat, QSettings::UserScope, "VBook");

void VBook::registerType() {
    qmlRegisterType<VBook, 1>("VBook", 1, 0, "VBook");
}

VBook::VBook(QObject *parent) : QObject(parent),
                                CurrentPage(new VPage()),
                                additionalSize(2){

}

void VBook::nextPage(QString choice) {
    delete CurrentPage;

    CurrentPage = new VPage(choice);

    body_image = "qrc" + ((CurrentPage->body_image.size()) ? CurrentPage->body_image : body_image_book);
    head_image = "qrc" + ((CurrentPage->head_image.size()) ? CurrentPage->head_image : head_image_book);
    bottom_image = "qrc" + ((CurrentPage->bottom_image.size()) ? CurrentPage->bottom_image : bottom_image_book);
    settings.setValue("currpage", choice);
    pageChanged();
}

void VBook::init() {
    QString l_path = path + "book_res/Inventory/";
    QStringList lst = QDir(l_path).entryList(QDir::Files);

    for(auto it : lst) {
        inv.push_back(new VInventory(l_path + it));
    }

    invSize = inv.size();

    QFile file;

    file.setFileName(path + "book_res/info.xml");

    if(!file.open(QIODevice::ReadOnly)) {
        return;
    }

    QDomDocument doc;
    if(!doc.setContent(&file)) {
        return;
    }

    QDomElement root = doc.documentElement();
    QString temp = root.firstChildElement("body_image").text();
    body_image_book = (temp.size()) ? path + "book_res/" + temp : "";

    temp = root.firstChildElement("head_image").text();
    head_image_book = (temp.size()) ? path + "book_res/" + temp : "";

    temp = root.firstChildElement("bot_image").text();
    bottom_image_book = (temp.size()) ? path + "book_res/" + temp : "";

    CurrentPage = new VPage(settings.value("currpage", "0").toString());

    body_image = "qrc" + ((CurrentPage->body_image.size()) ? CurrentPage->body_image : body_image_book);
    head_image = "qrc" + ((CurrentPage->head_image.size()) ? CurrentPage->head_image : head_image_book);
    bottom_image = "qrc" + ((CurrentPage->bottom_image.size()) ? CurrentPage->bottom_image : bottom_image_book);

    pageChanged();
    bookInventoryChanged();
}

QString VBook::pageText() {
    return CurrentPage->Text;
}

QList<QObject *> VBook::pageAnswers() {
    return CurrentPage->answers;
}

int VBook::answersSize()
{
    int ret = 0;
    for(auto it = CurrentPage->answers.begin(); it != CurrentPage->answers.end(); ++it) {
        if (((VAnswer*)*it)->isVisible) {
            ++ret;
        }
    }
    qDebug() << "[" << ret << "]";
    return ret;
}

QList<QObject*> VBook::additional()
{
    QList<QObject*> ret;
    ret.push_back(new additionalItem());
    ((additionalItem*)ret.back())->text = "edited text";
    ((additionalItem*)ret.back())->type = 0;
    ret.push_back(new additionalItem());
    ((additionalItem*)ret.back())->text = "just image";
    ((additionalItem*)ret.back())->type = 1;
    return ret;
}

QList<QObject *> VBook::inventory() {
    return inv;
}

QList<QObject *> VBook::bookInventory(int i)
{
    return ((VInventory*)inv[i])->items;
}

