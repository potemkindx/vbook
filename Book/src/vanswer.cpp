#include "vanswer.h"
#include "vbook.h"

#include <ctime>

#include <QFile>
#include <QDebug>
#include <QMap>
#include <cstdlib>

VAnswer::VAnswer(QString filename, QObject *parent)
               : QObject(parent),
                 isVisible(false),
                 isRandom(false),
                 doc("answer") {
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        return;
    }
    if (!doc.setContent(&file)) {
        file.close();
        return;
    }
    file.close();

    root = doc.documentElement();

    info = root.firstChildElement("info");
    visible = root.firstChildElement("visible");
    actions = root.firstChildElement("actions");
    type_conditions = root.firstChildElement("type_conditions");
    next_page = root.firstChildElement("next_page");

    isVisible = processConditions(visible);
    isRandom = (info.firstChildElement("type").text().toInt() == 2);
}

QString VAnswer::answerText() {
    return info.firstChildElement("text").text();
}


QString VAnswer::nextPage() {
    if(!perfomActions() ||
       next_page.isNull() ||
       info.isNull()) {
        return 0;
    }

    QDomElement next_elem = next_page.toElement();
    if(next_elem.isNull()) {
        return 0;
    }

    QDomElement info_elem = info.toElement();

    QString id  = next_elem.firstChildElement("id").text();
    QString alt = next_elem.firstChildElement("alternative").text();

    QDomElement type_elem = info_elem.firstChildElement("type");
    int type = 0;
    if(!type_elem.isNull()) {
        type = type_elem.text().toInt();
    }

    QString ret  = id;

    if(type == 2 && !processConditions(type_conditions)) {
        ret = alt;
    }

    return ret;

}

bool VAnswer::perfomActions() {
    if(actions.isNull()) {
        return true;
    }

    QDomElement a_elem = actions.toElement();

    QDomNode action = a_elem.firstChild();
    while (!action.isNull()) {
        performAction(action);
        action = action.nextSibling();
    }

    return true;
}

bool VAnswer::checkCondtion(const QDomNode &condition) {
    bool ret = false;
    QDomElement c_elem = condition.toElement();
    if(c_elem.isNull()) {
        return ret;
    }

    QDomNode item = c_elem.firstChildElement("item");
    if(item.isNull()) {
        return ret;
    }
    QString item_elem = item.toElement().text();

    QDomNode type = c_elem.firstChildElement("type");
    if(type.isNull()) {
        return ret;
    }
    int type_elem = type.toElement().text().toInt();

    QDomNode count = c_elem.firstChildElement("count");
    if(count.isNull()) {
        return ret;
    }
    double l_count = count.toElement().text().toDouble();

    if(VBook::items.find(item_elem) == VBook::items.end()) {
        VBook::items[item_elem] = new VItem();
    }

    double g_count = VBook::items[item_elem]->count().toDouble();

    switch(type_elem) {
    case 1: { ret = (g_count == l_count); break; }
    case 2: { ret = (g_count <  l_count); break; }
    case 3: { ret = (g_count >  l_count); break; }
    case 4: { ret = (g_count <= l_count); break; }
    case 5: { ret = (g_count >= l_count); break; }
    case 6: { ret = (g_count != l_count); break; }
    default:{ ret = false; break;                }
    }

    return ret;
}

void VAnswer::performAction(const QDomNode &action) {
    QDomElement a_elem = action.toElement();
    if(a_elem.isNull()) {
        return;
    }

    QDomElement item = a_elem.firstChildElement("item");
    if(item.isNull()) {
        return;
    }
    QString item_elem = item.text();

    QDomElement type = a_elem.firstChildElement("type");
    if(type.isNull()) {
        return;
    }
    int type_elem = type.text().toInt();

    QDomElement count = a_elem.firstChildElement("count");
    if(count.isNull()) {
        return;
    }

    double l_count = 0;
    if(count.attribute("form", "1").compare("1") == 0) {
        l_count = count.text().toDouble();
    } else {
        if(VBook::items.find(count.text()) == VBook::items.end()) {
            VBook::items[count.text()] = new VItem();
        }
        l_count = VBook::items[count.text()]->count().toDouble();
    }

    if(VBook::items.find(item_elem) == VBook::items.end()) {
        VBook::items[item_elem] = new VItem();
    }

    double g_count = VBook::items[item_elem]->count().toDouble();

    switch(type_elem) {
    case 1: { g_count =  l_count;         break; }
    case 2: { g_count += l_count;         break; }
    case 3: { g_count -= l_count;         break; }
    case 4: { g_count *= l_count;         break; }
    case 5: { g_count = (l_count) ? (g_count / l_count) : 0; break;}
    case 6: { g_count = (rand() % (int)l_count) + 1; break; }
    case 7: { g_count = time(NULL); break; }
    //case 8: { remember(l_count); break; }
    //case 9: { forget(l_count); break; }
    default:{ break; }
    }

    VBook::items[item_elem]->setCount(QString("%1").arg(g_count));
    VBook::settings.setValue(item_elem, g_count);
}

bool VAnswer::processConditions(const QDomNode &type) {
    if(type.isNull()) {
        return false;
    }
    QDomElement v_elem = type.toElement();

    QDomNode condition = v_elem.firstChild();
    while (!condition.isNull()) {
        if(!checkCondtion(condition)) {
            return false;
        }
        condition = condition.nextSibling();
    }
    return true;
}

