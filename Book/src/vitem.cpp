#include "vitem.h"

VItem::VItem(QObject *parent) : QObject(parent), isVisible(false), m_name(""), m_count(""), m_image("") {}

QString VItem::name() {
    return m_name;
}

QString VItem::count() {
    return m_count;
}

QString VItem::image() {
    return m_image;
}

void VItem::setName(QString name) {
    m_name = name;
    nameChanged();
}

void VItem::setCount(QString count) {
    m_count = count;
    countChanged();

    isVisibleChanged();
}

void VItem::setImagePath(QString path) {
    m_image = path;
    imageChanged();
}
