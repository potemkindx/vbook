#include "vpage.h"
#include "vanswer.h"
#include "globals.h"
#include "vbook.h"

#include <QFile>
#include <QDir>
#include <QTextStream>
#include <QDebug>
#include <QtXml>
#include <QStringRef>

QString VPage::Path = path;
QString VPage::textExtension = "/text.txt";
QString VPage::answersExtension = "/answers/";
QString VPage::infoExtension = "/info.xml";

VPage::VPage(QString Source) : mSource(Source) {
    QFile file(Path + mSource + textExtension);
    if(!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Error reading text Part of file" << file.fileName();
    } else {
        Text = file.readAll();
        file.close();
        Text.replace("\n", "<br>");
        Text.replace("\t", "&nbsp; &nbsp; ");
        Text.replace("  ", " &nbsp;");
        for(int begin = Text.indexOf("<var>"), end   = Text.indexOf("</var>");
            begin != -1 && end != -1;
            begin = Text.indexOf("<var>"), end   = Text.indexOf("</var>")) {

            QString sRef = Text.mid(begin+5, end - begin-5);
            if(VBook::items.find(sRef) != VBook::items.end()) {
                Text.replace(begin, end-begin + 6, VBook::items[sRef]->count());
            } else {
                Text.replace(begin, end-begin + 6, "0");
            }
        }


    }
    QDir dir(Path + mSource + answersExtension);
    if (!dir.exists()) {
        return;
    }

    QStringList list = dir.entryList(QDir::Files);

    answers.clear();

    for(auto it : list) {
        answers.push_back(new VAnswer(Path + mSource + answersExtension + it));
    }

    file.setFileName(Path + mSource + infoExtension);
    if(!file.open(QIODevice::ReadOnly)) {
        qDebug() << "no info file: " << file.fileName();
        return;
    }
    QDomDocument doc;
    if(!doc.setContent(&file)) {
        return;
    }
    QDomElement root = doc.documentElement();
    QString temp = root.firstChildElement("body_image").text();
    body_image = (temp.size()) ? path + mSource + "/" + temp : "";

    temp = root.firstChildElement("head_image").text();
    head_image = (temp.size()) ? path + mSource + "/" + temp : "";

    temp = root.firstChildElement("bot_image").text();
    bottom_image = (temp.size()) ? path + mSource + "/" + temp : "";
}

VPage::VPage() : Text(""), answers(), body_image(""), head_image(""), bottom_image(""){
}

