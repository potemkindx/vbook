#include "vinventory.h"

#include "vitem.h"
#include "vbook.h"

#include <QFile>
#include <QDebug>

VInventory::VInventory(QString filename, QObject *parent) : QObject(parent)
{
    QFile file(filename);
    if (!file.open(QIODevice::ReadOnly)) {
        return;
    }
    if (!doc.setContent(&file)) {
        file.close();
        return;
    }
    file.close();

    root = doc.documentElement();

    info = root.firstChildElement("info");
    QString visible = info.firstChildElement("type").text();
    isVisible = (visible.size() && visible.toInt() != 0);
    text = info.firstChildElement("name").text();

    QDomElement item = root.firstChildElement("item");
    while (!item.isNull()) {
        VItem * ptr = new VItem();

        QDomElement info = item.firstChildElement("info");
        ptr->setName(info.firstChildElement("name").text());
        ptr->setImagePath(info.firstChildElement("image").text());

        QString id = info.firstChildElement("id").text();
        ptr->setCount(VBook::settings.value(id, "").toString());

        items.push_back(ptr);

        VBook::items[id] = ptr;


        item = item.nextSiblingElement("item");
    }
}
