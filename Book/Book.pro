TEMPLATE = app

QT += qml quick xml

CONFIG += c++11
CONFIG += qtquickcompiler

HEADERS += \
    include/vpage.h \
    include/vbook.h \
    include/vitem.h \
    include/vanswer.h \
    include/vinventory.h \
    include/globals.h \
    include/additionalitem.h

INCLUDEPATH += include

DEPENDPATH += src

SOURCES += src/main.cpp \
    src/vpage.cpp \
    src/vbook.cpp \
    src/vitem.cpp \
    src/vanswer.cpp \
    src/vinventory.cpp \
    src/additionalitem.cpp

RESOURCES += qml/qml.qrc \
             res/book.qrc \
    res/menu.qrc


include(deployment.pri)


