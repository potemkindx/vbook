import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4

SplitView {
    anchors.fill: parent
    resizing: false;

    TableWithHide {
        model: (currentPage && currentPage.answers) ? currentPage.answers : 0

        dialog.title: "Enter ID for new Answer"
        button.text: "Add new Answer"
        addAction:    function (text) {currentPage.AddAnswer(text)     }
        removeAction: function (row)  {currentPage.RemoveSelected(row) }
        selectAction: function (row)  {currentPage.SelectAnswer(row)   }

        Component.onCompleted: tableHeader.append({ "role": "head", "title": "Answers" })
    }

    ColumnLayout {
        enabled: (currentAnswer) ? 1 : 0
        Layout.margins: 10
        id: mainTables
        spacing : 10;

        RowLayout {
            width: parent.width

            TextField {
                placeholderText: "QuestionText"
                Layout.fillWidth: true;
                text: (currentAnswer) ? currentAnswer.text : ""
                onTextChanged: {
                    if (currentAnswer) {
                        currentAnswer.text = text;
                    }
                }
            }
        }

        AnswerTable {
            id: precondTable
            items: (currentAnswer) ? currentAnswer.preconditionModel : 0
            Layout.fillWidth: true;
            Layout.fillHeight: true;
            headerText: "Preconditional table"
            onCurrentRowChanged: {
                console.log("qml Select Pre")
                currentAnswer.SelectPreCondition(currentRow)
            }

            delegateItem: TextEditDelegate {
                onTextChanged: currentAnswer.ChangePreCondition(0, text)
            }
            delegateType: CompareDelegate {
                onCurrentIndexChanged: currentAnswer.ChangePreCondition(1, currentIndex)
            }
            delegateCount: TextEditDelegate {
                onTextChanged: currentAnswer.ChangePreCondition(2, text)
            }

            addAction: function () {
                precondTable.currentRow = -1;
                currentAnswer.AddPreCondition();
            }

            removeAction: function() {
                currentAnswer.RemovePreCondition();
            }

        }

        AnswerTable {
            id: actionTable
            items: (currentAnswer) ? currentAnswer.actionModel : 0
            Layout.fillWidth: true;
            Layout.fillHeight: true;
            headerText: "Action table"
            delegateItem: TextEditDelegate {onTextChanged: {
                    currentAnswer.ChangeAction(actionTable.currentRow, 0, text)}
            }

            onCurrentRowChanged: currentAnswer.SelectAction(currentRow)

            delegateType: ActionDelegate {onCurrentIndexChanged: currentAnswer.ChangeAction(actionTable.currentRow, 1, currentIndex)}
            delegateCount: TextEditDelegate {onTextChanged: currentAnswer.ChangeAction(actionTable.currentRow, 2, text)}
            addAction: function () {
                actionTable.currentRow = -1;
                currentAnswer.AddAction();
            }
            removeAction: function() {
                currentAnswer.RemoveAction();
            }
        }

        AnswerTable {
            id: condtionalTable

            items: (currentAnswer) ? currentAnswer.conditionModel : 0
            Layout.fillWidth: true;
            Layout.fillHeight: true;
            headerText: "Answers table"
            onCurrentRowChanged: {
                currentAnswer.SelectPreCondition(currentRow)
            }
            delegateItem: TextEditDelegate {onTextChanged: currentAnswer.ChangeCondition(0, text)}
            delegateType: CompareDelegate {onCurrentIndexChanged: currentAnswer.ChangeCondition(1, currentIndex)}
            delegateCount: TextEditDelegate {onTextChanged: currentAnswer.ChangeCondition(2, text)}
            addAction: function () {
                condtionalTable.currentRow = -1;
                currentAnswer.AddCondition();
            }
            removeAction: function() {
                currentAnswer.RemoveCondition();
            }
        }

        RowLayout {
            width: parent.width

            TextField {
                placeholderText: "NextPage"
                Layout.fillWidth: true;
                text: (currentAnswer) ? currentAnswer.nextQuestion : ""
                onTextChanged: {
                    if (currentAnswer) {
                        currentAnswer.nextQuestion = text;
                    }
                }
            }
            TextField {
                placeholderText: "AlternativePage"
                Layout.fillWidth: true;
                visible: condtionalTable.rowCount
                text: (currentAnswer) ? currentAnswer.alternativeQuestion : ""
                onTextChanged: {
                    if (currentAnswer) {
                        currentAnswer.alternativeQuestion = text;
                    }
                }
            }
        }
    }
}

