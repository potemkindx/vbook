import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2


ToolBar  {
    FileDialog {
        id: newBookDialog
        title: "Please choose a dir for book"
        folder: shortcuts.home
        selectMultiple: false;
        selectFolder: true;
        onAccepted: {
            book.init(fileUrl);
        }
    }

    FileDialog {
        id: openBookDialog
        title: "Please choose an existed book"
        folder: shortcuts.home
        selectMultiple: false;
        nameFilters: [ "Book files (*.qrc)", "All files (*)" ]
        onAccepted: {
            book.load(fileUrl);
        }
    }
    RowLayout {

        width: parent.width
        ToolButton {
            Layout.fillWidth: true;
            text: "New"
            onClicked:  {
                newBookDialog.open()
            }
        }
        ToolButton {
            Layout.fillWidth: true;
            text: "Open"
            onClicked:  {
                openBookDialog.open()
            }
        }

        ToolButton {
            visible: book.isInit
            Layout.fillWidth: true;
            text: "Save"
            onClicked: {
                book.save()
            }
        }

        ToolButton {
            property bool startPage: true;
            visible: book.isInit
            Layout.fillWidth: true;
            text: "Edit Variables"
            onClicked: {
                if(startPage) {
                    pageLoader.source = "InventoryEditor.qml"
                } else {
                    pageLoader.source = "MainEditor.qml"
                }
                startPage = !startPage
            }
        }
    }
}
