import QtQuick 2.5
import QtQuick.Controls 1.4

ComboBox {
    model: ListModel {
        ListElement { text: "=" }
        ListElement { text: "+=" }
        ListElement { text: "-=" }
        ListElement { text: "*=" }
        ListElement { text: "/=" }
        ListElement { text: "random" }
        ListElement { text: "time" }
    }

    currentIndex: styleData.value
}
