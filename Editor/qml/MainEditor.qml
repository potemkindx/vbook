import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

import QtQuick.Dialogs 1.2

SplitView {
    anchors.fill: parent
    enabled: book.isInit
    resizing: false;

    TableWithHide {
        model: book.pages

        dialog.title: "Enter ID for new Page"
        button.text: "Add new Page"
        addAction:    function (text) {book.AddPage(text)   }
        selectAction: function (row)  {book.SelectPage(row) }
        removeAction: function (row)  {book.RemovePage(row) }

        Component.onCompleted: tableHeader.append({ "role": "head", "title": "Pages" })
    }

    TabView {
        id: tabView
        enabled: (currentPage) ? 1 : 0
        currentIndex: 0;

        Tab {
            id: textTab
            title: "Text"
            active: true;
            TextEditor {
                anchors.fill: parent
                anchors.margins: 10
            }
        }

        Tab {
            title: "Answers"
            active: true;

            Answers {}
        }

        Tab {
            id: pageSettingsTab
            title: "Settings"
            active: true;
            PageSettings {}
        }
    }

}
