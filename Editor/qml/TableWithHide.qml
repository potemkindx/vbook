import QtQuick 2.5
import QtQuick.Controls 1.4

Row {
    property alias model: baseTable.model

    property alias tableHeader: tableRowModel
    property alias dialog: newItemDiag
    property alias button: addItem

    property var selectAction: function (row)  {}
    property var removeAction: function (row)  {}
    property var addAction:    function (text) {}

    Component {
        id: columnComponent
        TableViewColumn {}
    }

    ListModel {
        id: tableRowModel
    }

    Column{
        id: items
        height: parent.height
        TableView {
            id: baseTable
            resources: {
                var roleList = tableRowModel
                var temp = []
                for(var i=0; i < tableRowModel.count; i++) {
                    var role  = tableRowModel.get(i)
                    temp.push(columnComponent.createObject(baseTable,
                                { "role": role.role, "title": role.title}))
                }
                return temp
            }

            height: parent.height - addItem.height
            onCurrentRowChanged: {
                selectAction(currentRow);
            }

            Keys.onDeletePressed: {
                removeAction(currentRow);
            }
        }


        TextInputDialog {
            id: newItemDiag
            title: "Add New Something"
            onAccepted: {addAction(textString)}
        }

        Button {
            id: addItem;
            width: parent.width
            text: "Add New Item"

            onClicked: newItemDiag.open();
        }
    }

    Button {
        property double prevWidth;
        width: 20
        height: parent.height;
        text:"h"
        onClicked: {
            if(items.visible) {
                baseTable.visible = false;
                items.visible = false;
                width = 50;
                text = "show"
            } else {
                items.visible = true
                baseTable.visible = true
                width = 20;
                text = "h"
            }
        }

    }
}

