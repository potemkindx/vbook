import QtQuick 2.5
import QtQuick.Dialogs 1.2

FileDialog {
    id: diag;
    title: "chose image file";
    selectMultiple: false;
    nameFilters: [ "Image files (*.png *.jpg *.svg *.bmp)", "All files (*)" ]
}
