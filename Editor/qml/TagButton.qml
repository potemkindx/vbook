import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Rectangle {
    property string ttext: "Bold"
    property string tag: "<b>"
    property string close_tag: "</b>"
    property TextArea context_elem: TextArea {}

    Label {
        id: label
        anchors.centerIn: parent
        text: tag + ttext + close_tag
        color: "black"
        font.pixelSize: 24;
    }

    color: "lightblue"; radius: 10.0

    MouseArea {
        anchors.fill: parent
        onClicked: {
            if(context_elem.selectionStart >= context_elem.selectionEnd) {
                return;
            }

            var start = context_elem.selectionStart;
            var end = context_elem.selectionEnd;
            console.log("hello " + start +
                             " " + end)
            var text = context_elem.text
            context_elem.text = [text.slice(0, start), tag, text.slice(start)].join('');
            end += tag.length
            text = context_elem.text
            context_elem.text = [text.slice(0, end), close_tag, text.slice(end)].join('');
            console.log("after: " + start + " " + end);
        }
    }
}
