import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Dialogs 1.2

Dialog {
    property alias textString: textField.text
    visible: false
    title: "Blue sky dialog"
    standardButtons: StandardButton.Ok | StandardButton.Cancel
    width: 250;
    height: 70

    TextField {
        id: textField;
        width: parent.width
        text: "Hello Dude!"
    }

}
