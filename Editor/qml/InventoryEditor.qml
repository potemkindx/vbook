import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

SplitView {
    resizing: false;
    anchors.fill: parent
    enabled: book.isInit

    TableWithHide {
        model: book.inventories

        dialog.title: "Enter name for new Inventory"
        button.text: "Add new Inventory"
        addAction:    function (text) {book.AddInventory(text)   }
        selectAction: function (row)  {book.SelectInventory(row) }
        removeAction: function (row)  {book.RemoveInventory(row) }

        Component.onCompleted: tableHeader.append({ "role": "head", "title": "Inventory" })

    }

    TableWithHide {
        enabled: currentInventory

        model: (currentInventory) ? currentInventory.items : 0

        dialog.title: "Enter name for new Item"
        button.text: "Add new Item"
        addAction:    function (text) {currentInventory.AddItem(text)   }
        selectAction: function (row)  {currentInventory.SelectItem(row) }
        removeAction: function (row)  {currentInventory.RemoveItem(row) }

        Component.onCompleted: tableHeader.append({ "role": "head", "title": "Items" })
    }

    ItemsTable {}
}

