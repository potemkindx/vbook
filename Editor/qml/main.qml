import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Controls.Styles 1.4
import QtQuick.Dialogs 1.2

import Book 1.0

ApplicationWindow {
    id: windows
    visible: true
    minimumHeight: 768
    minimumWidth: 1024

    Book {id: book}

    property var currentPage: book.currentPage
    property var currentAnswer: (currentPage) ? book.currentPage.currentAnswer : 0
    property var currentInventory: book.currentInventory
    property var currentItem: (currentInventory) ? currentInventory.currentItem : 0

    toolBar: MainToolBar {}

    Loader {
        anchors.fill: parent;
        id: pageLoader
        source: "MainEditor.qml"
        //source: "InventoryEditor.qml"
        active: true;
        focus: true
    }

    statusBar: StatusBar {}


}

