import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    id: itemsTable
    enabled: (currentItem) ? 1 : 0
    Layout.fillWidth: true
    spacing : 10;
    Layout.margins: 10

    Label {
        Layout.fillWidth: true;
        horizontalAlignment: Text.AlignHCenter
        text: "User displayed name"
    }

    TextField {
        placeholderText: "name"
        Layout.fillWidth: true;
        text: (currentItem) ? currentItem.name : ""
        onTextChanged: {
            if (currentItem) {
                currentItem.name = text;
            }
        }
    }

    Label {
        Layout.fillWidth: true;
        horizontalAlignment: Text.AlignHCenter
        text: "Description on popup"
    }

    TextField {
        placeholderText: "description"
        Layout.fillWidth: true;
        text: (currentItem) ? currentItem.description : ""
        onTextChanged: {
            if (currentItem) {
                currentItem.description = text;
            }
        }
    }

    Label {
        Layout.fillWidth: true;
        horizontalAlignment: Text.AlignHCenter
        text: "Start count of items"
    }

    TextField {
        placeholderText: "count"
        Layout.fillWidth: true;
        text: (currentItem) ? currentItem.count : ""
        onTextChanged: {
            if (currentItem) {
                currentItem.count = text;
            }
        }
    }

    Label {
        Layout.fillWidth: true;
        horizontalAlignment: Text.AlignHCenter
        text: "Minimal count of items for displaying"
    }

    TextField {
        placeholderText: "minimum"
        Layout.fillWidth: true;
        text: (currentItem) ? currentItem.minimum : ""
        onTextChanged: {
            if (currentItem) {
                currentItem.minimum = text;
            }
            console.log("hmm?")
            hidden.checked = currentInventory.isHidden;
        }
    }

    Repeater {
        model: 3
        Label {
            Layout.fillWidth: true;
            text: ""
        }
    }


    CheckBox {
        id: hidden
        text: "Hide Inventory"
        checked: (currentInventory) ? currentInventory.isHidden : 0
        anchors.horizontalCenter: parent.horizontalCenter
        onCheckedChanged: {
            currentInventory.isHidden = checked
        }

        function invChanged() {hidden.checked = currentInventory.isHidden}

        Component.onCompleted: {
            book.currentInventoryChanged.connect(invChanged)
        }
        Component.onDestruction: {
            book.currentInventoryChanged.disconnect(invChanged)
        }
    }


}
