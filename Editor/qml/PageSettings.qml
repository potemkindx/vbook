import QtQuick 2.5
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
Row {
    id: baseElement
    anchors.margins: 10
    anchors.fill: parent
    spacing: 10
    ImageDialog {
        id: top_dialog;
        onAccepted: {currentPage.topImage = fileUrl;}
    }

    ImageDialog {
        id: body_dialog;
        onAccepted: {currentPage.bodyImage = fileUrl;}
    }

    ImageDialog {
        id: bottom_dialog;
        onAccepted: {currentPage.bottomImage = fileUrl;}
    }

    Column {
        height: parent.height;
        width: 2 *parent.width / 3;
        spacing: 1
        property double visibleHeight: height - (header.height) * header.visible
        Row {
            height: parent.height / 14;
            width: parent.width;
            id: header;
            visible: !hideTop.checked
            spacing: 1
            Repeater {
                model: 3
                Rectangle {
                    width: (header.width - header.spacing * 3)/ 3
                    height: header.height
                    border.color: "black"
                    border.width: 1
                    color: "grey"
                    Text {
                        anchors.centerIn: parent
                        text: "Header"
                    }
                }
            }
        }



        Rectangle {
            height: parent.visibleHeight / 5;
            width: parent.width;
            Layout.fillHeight: true;
            border.color: "grey"
            border.width: 1


            Text {
                anchors.centerIn: parent
                text: "Click to chose Top Image"
                visible: !top_image.source.length
            }

            Image {
                id: top_image;
                anchors.fill: parent;
                source: (currentPage && currentPage.topImage.length)
                        ? ("file:" + currentPage.topImage ): ''
            }


            MouseArea {
                anchors.fill: parent;
                onClicked: top_dialog.open();
            }
        }

        Rectangle {
            height: 3*parent.visibleHeight / 5;
            Layout.fillHeight: true;
            width: parent.width;
            border.color: "grey"
            border.width: 1

            Text {
                anchors.centerIn: parent
                text: "Click to chose Body Image"
                visible: !body_image.source.length
            }

            Image {
                id: body_image;
                anchors.fill: parent;
                source: (currentPage && currentPage.bodyImage.length)
                        ? ("file:" + currentPage.bodyImage) : ''
            }

            MouseArea {
                anchors.fill: parent;
                onClicked: body_dialog.open();
            }
        }

        Rectangle {
            height: parent.visibleHeight / 5;
            Layout.fillHeight: true;
            width: parent.width;
            border.color: "grey"
            border.width: 1

            Text {
                anchors.centerIn: parent
                text: "Click to chose Bottom Image"
                visible: !bottom_image.source.length
            }

            Image {
                id: bottom_image;
                anchors.fill: parent;
                source: (currentPage && currentPage.bottomImage.length)
                        ? ("file:" + currentPage.bottomImage) : ''
            }

            MouseArea {
                anchors.fill: parent;
                onClicked: bottom_dialog.open();
            }
        }
    }
    Column {
        width: parent.width / 3 - 10

        spacing: 10;
        CheckBox {
            id: hideTop
            checked: (currentPage) ? !currentPage.visibleHeader : 0
            anchors.horizontalCenter: parent.horizontalCenter
            text: "Hide menu toolbar"
            onCheckedChanged: {
                header.visible = !checked
                currentPage.visibleHeader = !checked
            }

            function pageChanged() {hideTop.checked = !currentPage.visibleHeader}

            Component.onCompleted: {
                book.currentPageChanged.connect(pageChanged)
            }
            Component.onDestruction: {
                book.currentPageChanged.disconnect(pageChanged)
            }
        }

        Rectangle {
            height: baseElement.height - hideTop.height - 10
            border.color: "black"
            color: "transparent"
            width: parent.width
            Label {
                anchors.centerIn: parent
                text: "Место для вашей рекламы"
            }
        }
    }
}
