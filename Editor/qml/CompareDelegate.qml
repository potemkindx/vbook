import QtQuick 2.5
import QtQuick.Controls 1.4

ComboBox {
    property int currIndex : 0;
    model: ListModel {
        ListElement { text: "==" }
        ListElement { text: "<" }
        ListElement { text: ">" }
        ListElement { text: "<=" }
        ListElement { text: ">=" }
        ListElement { text: "!=" }
    }
    currentIndex: styleData.value
}

