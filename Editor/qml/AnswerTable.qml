import QtQuick 2.5
import QtQuick.Controls 1.4

Item {
    property var items: ListModel {}

    property Component delegateType : TextEditDelegate {}
    property Component delegateItem : CompareDelegate {}
    property Component delegateCount : TextEditDelegate {}

    property string headerText: ""

    property alias currentRow: table.currentRow
    property alias rowCount: table.rowCount
    property var addAction: function () {}
    property var removeAction: function () {}

    Text {
        id:header;
        anchors.top: parent.top;
        anchors.left: parent.left;
        anchors.right: parent.right;
        text: headerText
        horizontalAlignment: Text.AlignHCenter
    }

    TableView {

        id: table;
        anchors.top: header.bottom;
        anchors.left: parent.left;
        anchors.right: parent.right;
        anchors.bottom: buttons.top;
        TableViewColumn{ width: table.width / 3 - 1; role: "item" ;
                         title: "Item";   delegate: delegateItem}
        TableViewColumn{ width: table.width / 3 - 1; role: "type" ;
                         title: "Type";   delegate: delegateType}
        TableViewColumn{ width: table.width / 3 - 1; role: "count" ;
                         title: "Count"; delegate: delegateCount}

        model: items
    }

    Row {
        id: buttons;
        anchors.bottom: parent.bottom
        anchors.left: parent.left;
        anchors.right: parent.right;
        spacing: parent.width * 0.047
        Button {
            width: parent.width / 2.1
            text: "Add";
            onClicked: { addAction();}

        }
        Button {
            width: parent.width / 2.1
            text: "Remove"
            onClicked: removeAction();
        }
    }
}

