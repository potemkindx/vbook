#ifndef ITEM_H
#define ITEM_H

#include <QObject>

class Item : public QObject
{
    Q_OBJECT
public:
    explicit Item(QString head, QObject *parent = 0);

    Q_PROPERTY(QString description MEMBER m_description NOTIFY ItemChanged)
    Q_PROPERTY(QString minimum     MEMBER m_min         NOTIFY ItemChanged)
    Q_PROPERTY(QString count       MEMBER m_count       NOTIFY ItemChanged)
    Q_PROPERTY(QString head        MEMBER m_head        NOTIFY ItemChanged)
    Q_PROPERTY(QString name        MEMBER m_name        NOTIFY ItemChanged)

signals:
    void ItemChanged();
public:
    QString m_description;
    QString m_count;
    QString m_head;
    QString m_name;
    QString m_min;
};

#endif // ITEM_H
