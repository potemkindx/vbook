#ifndef ACTION_H
#define ACTION_H

#include <QObject>
#include <QAbstractListModel>

class Action: public QObject
{
    Q_OBJECT
public:
    Action(const QString & item, const int &type, const QString &count);
    Q_PROPERTY(int type MEMBER m_type NOTIFY ActionChanged)
    Q_PROPERTY(QString item MEMBER m_item NOTIFY ActionChanged)
    Q_PROPERTY(QString count MEMBER m_count NOTIFY ActionChanged)
signals:
    void ActionChanged();
public:
    int m_type;
    QString m_item;
    QString m_count;
};

#endif // ACTION_H
