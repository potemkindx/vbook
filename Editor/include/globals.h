#ifndef GLOBALS_H
#define GLOBALS_H
#include <QString>
#include <QVector>
#include <QMap>

class Globals {
    Globals();
public:
    static class QVector < QString > Records;
    static class QMap < QString, QVector < QString > > Objects;
    static class QFile *log_file;
    static void installQtMessageHandler();
};

#endif // GLOBALS_H
