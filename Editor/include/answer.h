#ifndef ANSWER_H
#define ANSWER_H

#include <QObject>
#include <QVector>
#include "xmlhelper.h"
#include "action.h"
class Answer : public QObject, public xmlHelper {
    Q_OBJECT
public:
    explicit Answer(QString Head, QObject *parent = 0);

    Q_PROPERTY(QString head READ head NOTIFY mHeadChanged)
    Q_PROPERTY(QString text MEMBER mText NOTIFY mTextChanged)
    Q_PROPERTY(QString nextQuestion MEMBER mNextQuestion NOTIFY mNextQuestionChanged)
    Q_PROPERTY(QString alternativeQuestion MEMBER mAlternativeQuestion NOTIFY mAlternativeQuestionChanged)

    Q_PROPERTY(QList < QObject * > actionModel MEMBER mActionModel NOTIFY actionModelChanged)
    Q_PROPERTY(QList < QObject * > conditionModel MEMBER mConditionModel NOTIFY conditionalModelChanged)
    Q_PROPERTY(QList < QObject * > preconditionModel MEMBER mPreConditionModel NOTIFY preConditionalModelChanged)
signals:
    void mHeadChanged();
    void mTextChanged();
    void mNextQuestionChanged();
    void mAlternativeQuestionChanged();
    void actionModelChanged();
    void conditionalModelChanged();
    void preConditionalModelChanged();
public slots:

    void ChangeAction(int col, QVariant value);
    void SelectAction(int row);
    void AddAction();
    void RemoveAction();

    void ChangeCondition(int col, QVariant value);
    void SelectCondition(int row);
    void AddCondition();
    void RemoveCondition();

    void ChangePreCondition(int col, QVariant value);
    void SelectPreCondition(int row);
    void AddPreCondition();
    void RemovePreCondition();
public:
    QString head() { return mHead; }
    void Save(QString path);
    void Load(QString path);
private:
    void SaveModel(QList < QObject * > & model, class QDomElement * parent, QString Name);
    QString mHead;
    QString mText;
    QString mNextQuestion;
    QString mAlternativeQuestion;
    QList < QObject * > mActionModel;
    QList < QObject * > mConditionModel;
    QList < QObject * > mPreConditionModel;
    Action * mCurrentAction;
    Action * mCurrentCondition;
    Action * mCurrentPreCondition;
};

#endif // ANSWER_H
