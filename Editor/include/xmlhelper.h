#ifndef XMLHELPER_H
#define XMLHELPER_H

#include <QDomDocument>
#include <QFile>

class xmlHelper {
public:
    xmlHelper();
protected:

    void AddChildValue(class QDomElement &parent, QString Name, QString Value);
    void AddImage(class QDomElement& parent, QString image, QString name, QString Path);
    bool Init(QString Path);
    void Finish(class QDomElement& child);
    void InitReading(QString path);
    void FinishReading();
    QDomDocument doc;
private:
    QFile xmlFile;
};

#endif // XMLHELPER_H
