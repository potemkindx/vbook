#ifndef INVENTORY_H
#define INVENTORY_H

#include <QObject>
#include "xmlhelper.h"

class Inventory : public QObject, public xmlHelper {
    Q_OBJECT
public:
    explicit Inventory(QString head, QObject *parent = 0);
    Q_PROPERTY(QList<QObject*> items READ items NOTIFY itemsChanged)
    Q_PROPERTY(QObject * currentItem READ currentItem NOTIFY currentItemChanged)

    Q_PROPERTY(QString head     MEMBER mHead     NOTIFY inventoryChanged)
    Q_PROPERTY(QString name     MEMBER mName     NOTIFY inventoryChanged)
    Q_PROPERTY(bool    isHidden MEMBER mIsHidden NOTIFY isHiddentChanged)
signals:
    void itemsChanged();
    void currentItemChanged();
    void inventoryChanged();
    void isHiddentChanged();
public slots:
    QList<QObject*> items();
    void AddItem(QString text);
    void SelectItem(int index);
    void RemoveItem(int index);
    QObject* currentItem();
public:
    void Save(QString path);
    void Load(QString path);
private:
    QList<QObject*> mItems;
    QObject * mCurrentItem;
    bool mIsHidden;
    QString mHead;
    QString mName;
};

#endif // INVENTORY_H
