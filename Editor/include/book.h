#ifndef BOOK_H
#define BOOK_H

#include <QObject>
#include "xmlhelper.h"
class Book : public QObject, public xmlHelper
{
    Q_OBJECT
public:
    explicit Book(QObject *parent = 0);
    Q_PROPERTY(QList<QObject*> pages READ pages NOTIFY pagesChanged)
    Q_PROPERTY(QObject* currentPage READ currentPage NOTIFY currentPageChanged)
    Q_PROPERTY(QList<QObject*> inventories READ inventories NOTIFY inventoriesChanged)
    Q_PROPERTY(QObject* currentInventory READ currentInventory NOTIFY currentInventoryChanged)
    Q_PROPERTY(bool isInit MEMBER mIsInit NOTIFY isInitChanged)
    static void registerType();
signals:
    void pagesChanged();
    void currentPageChanged();
    void isInitChanged();
    void inventoriesChanged();
    void currentInventoryChanged();
public slots:
    void AddPage(QString text);
    void SelectPage(int index);
    void RemovePage(int index);
    QList<QObject*> pages();
    QObject* currentPage();

    void AddInventory(QString text);
    void SelectInventory(int index);
    void RemoveInventory(int index);
    QList<QObject*> inventories();
    QObject* currentInventory();

    void init(QString path);
    void load(QString path);
    void save();
private:
    QList<QObject*> mPages;
    QObject*  mCurrentPage;
    QList<QObject*> mInventories;
    QObject*  mCurrentInventory;

    QString mPath;
    bool mIsInit;
};

#endif // BOOK_H
