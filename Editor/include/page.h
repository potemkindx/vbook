#ifndef PAGE_H
#define PAGE_H

#include <QObject>
#include "xmlhelper.h"

class Page : public QObject, public xmlHelper
{
    Q_OBJECT
public:
    explicit Page(QString head, QObject *parent = 0);
    Q_PROPERTY(QList<QObject*> answers READ answers NOTIFY answersChanged)
    Q_PROPERTY(QObject * currentAnswer READ currentAnswer NOTIFY currentAnswerChanged)
    Q_PROPERTY(QString head MEMBER mHead NOTIFY mHeadChanged)
    Q_PROPERTY(QString text READ text WRITE setText NOTIFY textChanged)
    Q_PROPERTY(int currentIndex MEMBER mCurrentIndex NOTIFY currentIndexChanged)
    Q_PROPERTY(QString topImage MEMBER mTopImage WRITE setTopImage NOTIFY topImageChanged)
    Q_PROPERTY(QString bodyImage MEMBER mBodyImage WRITE setBodyImage NOTIFY bodyImageChanged)
    Q_PROPERTY(QString bottomImage MEMBER mBottomImage WRITE setBottomImage NOTIFY bottomImageChanged)
    Q_PROPERTY(bool visibleHeader MEMBER mHeaderVisible NOTIFY headerVisibleChanged)
signals:
    void mHeadChanged();
    void textChanged();
    void answersChanged();
    void currentAnswerChanged();
    void topImageChanged();
    void currentIndexChanged();
    void bodyImageChanged();
    void bottomImageChanged();
    void headerVisibleChanged();
public slots:
    QString text();
    void setText(QString text);

    QList<QObject*> answers();
    void AddAnswer(QString text);
    void SelectAnswer(int index);
    void RemoveSelected(int index);
    QObject* currentAnswer();

    void setTopImage(QString path);
    void setBodyImage(QString path);
    void setBottomImage(QString path);

    void Save(QString path);
    void Load(QString path);
public:
    QString mText;
    QString mTopImage;
    QString mBodyImage;
    QString mBottomImage;
    QString mHead;
    QList<QObject*> mAnswers;
    QObject* mCurrentAnswer;
    int mCurrentIndex;
    bool mHeaderVisible;
};

#endif // PAGE_H
