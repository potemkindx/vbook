#include "inventory.h"
#include "item.h"
#include "globals.h"

#include <QDebug>
#include <QFile>


Inventory::Inventory(QString head, QObject *parent)
    : QObject(parent), mItems(), mCurrentItem(nullptr),
      mIsHidden(false), mHead(head) {

}

QList<QObject *> Inventory::items() {
    return mItems;
}

void Inventory::AddItem(QString text) {
    if (text.toUpper() == "INFO") {
        qDebug() << "Reserved Name";
        return;
    }

    for(auto it = mItems.begin(); it != mItems.end(); ++it) {
        if(((Item * )(*it))->m_head.toUpper() == text.toUpper()) {
            qDebug() << "the same item";
            return;
        }
    }

    Item * ptr = new Item(text);
    mItems.append(ptr);
    itemsChanged();
}

void Inventory::SelectItem(int index) {
    if(index < 0 || index >= mItems.size()) {
        return;
    }
    mCurrentItem = mItems.at(index);

    currentItemChanged();
}

void Inventory::RemoveItem(int index) {
    if(index < 0 || index >= mItems.size()) {
        return;
    }

    mItems.removeAt(index);;
    itemsChanged();
}

QObject *Inventory::currentItem() {
    return mCurrentItem;
}

void Inventory::Save(QString path) {
    QString InvPath = QString("%1/%2.xml").arg(path).arg(mHead);
    if(!Init(InvPath)) {
        return;
    }

    QDomElement Inventory = doc.createElement("inventory");

    {
        QDomElement info = doc.createElement("info");
        AddChildValue(info, "name", mHead);
        AddChildValue(info, "visible", (mIsHidden) ? "0" : "1");
        Inventory.appendChild(info);
    }

    for(auto it = mItems.begin(); it != mItems.end(); ++it) {
        Item * ptr = (Item *)(*it);
        QDomElement item = doc.createElement("item");

        AddChildValue(item, "id", ptr->m_head);
        AddChildValue(item, "name", ptr->m_name);
        AddChildValue(item, "desciption", ptr->m_description);
        AddChildValue(item, "count", ptr->m_count);
        AddChildValue(item, "visible_count", ptr->m_min);

        Inventory.appendChild(item);
    }

    Finish(Inventory);
}

void Inventory::Load(QString path) {
    InitReading(path + mHead);
    QDomElement inventory = doc.documentElement();
    QDomElement info = inventory.firstChildElement("info");
    mName = info.firstChildElement("name").text();
    mHead = mName;

    mIsHidden = !info.firstChildElement("visible").text().toInt();

    QDomElement item = inventory.firstChildElement("item");

    while(!item.isNull()) {
        QString ID = item.firstChildElement("id").text();
        if(ID.size()) {
            Item * ptr = new Item(ID);

            ptr->m_name = item.firstChildElement("name").text();
            ptr->m_description = item.firstChildElement("description").text();
            ptr->m_count = item.firstChildElement("count").text();
            ptr->m_min = item.firstChildElement("visible_count").text();

            mItems.push_back(ptr);
        }
        item = item.nextSiblingElement("item");
    }
}
