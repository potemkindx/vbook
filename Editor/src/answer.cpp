#include "answer.h"
#include <QDebug>
#include <QFile>
#include <QDir>
#include <QDomDocument>
#include <globals.h>

Answer::Answer(QString Head, QObject *parent)
       : QObject(parent), xmlHelper(), mHead(Head),
         mActionModel(), mConditionModel(), mPreConditionModel(),
         mCurrentAction(nullptr), mCurrentCondition(nullptr),
         mCurrentPreCondition(nullptr) {
}

void Answer::Save(QString path) {
    QString AnswerPath = QString("%1/answers").arg(path);
    QDir dir(AnswerPath);
    if(!dir.mkpath(dir.absolutePath())) {
        qDebug() << "error on path " << AnswerPath;
    }

    if(!Init(QString("%1/%2.xml").arg(AnswerPath).arg(mHead))) {
        return;
    }

    QDomElement answer = doc.createElement("answer");
    {
        QDomElement info = doc.createElement("info");

        AddChildValue(info, "id", mHead);
        AddChildValue(info, "type", QString("%1").arg("2"));
        AddChildValue(info, "text", mText);

        answer.appendChild(info);
    }

    {
        QDomElement visible = doc.createElement("visible");
        SaveModel(mPreConditionModel, &visible, "condition");
        answer.appendChild(visible);
    }

    {
        QDomElement actions = doc.createElement("actions");
        SaveModel(mActionModel, &actions, "action");
        answer.appendChild(actions);
    }

    {
        QDomElement type_conditions = doc.createElement("type_conditions");
        SaveModel(mConditionModel, &type_conditions, "condition");
        answer.appendChild(type_conditions);
    }

    {
        QDomElement next_page = doc.createElement("next_page");
        AddChildValue(next_page, "id", mNextQuestion);
        AddChildValue(next_page, "alternative",
                      mAlternativeQuestion.size() ? mAlternativeQuestion : mNextQuestion);
        answer.appendChild(next_page);
    }

    Finish(answer);
}

void Answer::Load(QString path) {
    InitReading(path + "/" + mHead);

    QDomElement answer = doc.documentElement();
    QDomElement info = answer.firstChildElement("info");

    mHead = info.firstChildElement("id").text();
    mText = info.firstChildElement("text").text();

    QDomElement visible = answer.firstChildElement("visible");

    QDomElement condition = visible.firstChildElement("condition");
    while(!condition.isNull()) {
        Action * ptr = new Action("", 0, "");
        ptr->m_item = condition.firstChildElement("item").text();
        ptr->m_type = condition.firstChildElement("type").text().toInt();
        ptr->m_count = condition.firstChildElement("count").text();
        mPreConditionModel.push_back(ptr);

        condition = condition.nextSiblingElement("condition");
    }

    QDomElement actions = answer.firstChildElement("actions");
    QDomElement action = actions.firstChildElement("action");
    while(!action.isNull()) {
        Action * ptr = new Action("", 0, "");
        ptr->m_item = action.firstChildElement("item").text();
        ptr->m_type = action.firstChildElement("type").text().toInt();
        ptr->m_count = action.firstChildElement("count").text();
        mActionModel.push_back(ptr);
        action = action.nextSiblingElement("action");
    }

    QDomElement type_conditions = answer.firstChildElement("type_conditions");
    condition = type_conditions.firstChildElement("condition");
    while(!condition.isNull()) {
        Action * ptr = new Action("", 0, "");
        ptr->m_item = condition.firstChildElement("item").text();
        ptr->m_type = condition.firstChildElement("type").text().toInt();
        ptr->m_count = condition.firstChildElement("count").text();

        mConditionModel.push_back(ptr);
        condition = condition.nextSiblingElement("condition");
    }

    QDomElement next_page = answer.firstChildElement("next_page");
    mNextQuestion = next_page.firstChildElement("id").text();
    mAlternativeQuestion = next_page.firstChildElement("alternative").text();
}


void Answer::SaveModel(QList<QObject *> &model, QDomElement *parent, QString Name) {
    for(auto it = model.begin(); it != model.end(); ++it) {

        QDomElement stemElement = doc.createElement(Name);
        AddChildValue(stemElement, "item", ((Action *)*it)->m_item);
        AddChildValue(stemElement, "type", QString("%1").arg(((Action *)*it)->m_type));


        QDomElement count = doc.createElement("count");
        bool form = false;
        bool check = false;
        QString str;
        str.toInt();
        ((Action *)*it)->m_count.toDouble(&check);
        form |= check;
        ((Action *)*it)->m_count.toInt(&check);
        form |= check;
        count.setAttribute("form", QString("%1").arg(1 + !form));
        QDomText countText = doc.createTextNode(((Action *)*it)->m_count);
        count.appendChild(countText);

        stemElement.appendChild(count);

        parent->appendChild(stemElement);
    }
}


void Answer::AddAction() {
    mActionModel.push_back(new Action("foo", 3, "bar"));
    actionModelChanged();
}

void Answer::RemoveAction() {
    mActionModel.removeOne(mCurrentAction);
    actionModelChanged();
}

void Answer::AddCondition() {
    mConditionModel.push_back(new Action("foo", 3, "bar"));
    conditionalModelChanged();
}

void Answer::RemoveCondition() {
    mConditionModel.removeOne(mCurrentCondition);
    conditionalModelChanged();
}

void Answer::AddPreCondition() {
    mPreConditionModel.push_back(new Action("foo", 3, "bar"));
    preConditionalModelChanged();
}

void Answer::RemovePreCondition() {
    mPreConditionModel.removeOne(mCurrentPreCondition);
    preConditionalModelChanged();
}


void Answer::ChangeAction(int col, QVariant value) {
    if(!mCurrentAction){
        return;
    }
    switch(col) {
    case 0: mCurrentAction->m_item = value.toString(); break;
    case 1: mCurrentAction->m_type = value.toInt(); break;
    case 2: mCurrentAction->m_count = value.toString(); break;
    default: qDebug() << "Change Action wrong col"; break;
    }
    mCurrentAction->ActionChanged();
}

void Answer::SelectAction(int row) {
    if(row == -1) {
        mCurrentAction = nullptr;
        return;
    }
    if(mActionModel.size() <= row) {
        return ;
    }
    mCurrentAction = (Action*)mActionModel.at(row);
}

void Answer::ChangeCondition(int col, QVariant value) {
    if(!mCurrentCondition){
        return;
    }
    switch(col) {
    case 0: mCurrentCondition->m_item = value.toString(); break;
    case 1: mCurrentCondition->m_type = value.toInt(); break;
    case 2: mCurrentCondition->m_count = value.toString(); break;
    default: qDebug() << "Change Condition wrong col"; break;
    }
    mCurrentCondition->ActionChanged();
}

void Answer::SelectCondition(int row){
    if(row == -1) {
        mCurrentCondition = nullptr;
        return;
    }
    if(mConditionModel.size() <= row) {
        return ;
    }
    mCurrentCondition = (Action*)mConditionModel.at(row);
}

void Answer::ChangePreCondition(int col, QVariant value) {
    if(!mCurrentPreCondition){
        return;
    }

    switch(col) {
    case 0: mCurrentPreCondition->m_item = value.toString(); break;
    case 1: mCurrentPreCondition->m_type = value.toInt(); break;
    case 2: mCurrentPreCondition->m_count = value.toString(); break;
    default: qDebug() << "Change PreCondition wrong col"; break;
    }
    mCurrentPreCondition->ActionChanged();
}

void Answer::SelectPreCondition(int row) {
    if(row == -1) {
        mCurrentPreCondition = nullptr;
        return;
    }
    if( mPreConditionModel.size() <= row) {
        return ;
    }
    mCurrentPreCondition = (Action*)mPreConditionModel.at(row);
}



