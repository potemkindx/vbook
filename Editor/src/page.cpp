#include "page.h"
#include "answer.h"
#include <QDebug>
#include <QFile>
#include <QDir>
#include <globals.h>

Page::Page(QString head, QObject *parent)
     : QObject(parent), mHead(head), mAnswers(), mCurrentAnswer(NULL), mCurrentIndex(-1),
    mHeaderVisible(true) {
}

QString Page::text() {
    return mText;
}

void Page::setText(QString text) {
    if(mText != text) {
        mText = text;
        textChanged();
    }
}

QList<QObject *> Page::answers() {
    return mAnswers;
}

void Page::AddAnswer(QString text) {
    if (text.toUpper() == "INFO") {
        qDebug() << "Reserved Name";
        return;
    }

    for(auto it = mAnswers.begin(); it != mAnswers.end(); ++it) {
        if(((Answer * )(*it))->head().toUpper() == text.toUpper()) {
            qDebug() << "the same answer";
            return;
        }
    }
    Answer * ptr = new Answer(text);
    mAnswers.append(ptr);
    answersChanged();
}

void Page::SelectAnswer(int index) {
    if(index < 0 || index >= mAnswers.size() || index == mCurrentIndex) {
        return;
    }
    mCurrentAnswer = mAnswers.at(index);

    currentAnswerChanged();

    mCurrentIndex = index;
    currentIndexChanged();
}

void Page::RemoveSelected(int index) {
    if(index < 0 || index >= mAnswers.size() || mAnswers.empty()) {
        return;
    }
    mAnswers.removeAt(index);
    mCurrentIndex = -1;
    answersChanged();
}

QObject *Page::currentAnswer() {
    return mCurrentAnswer;
}

void Page::Save(QString path) {
    QString PagePath = QString("%1/%2").arg(path).arg(mHead);

    QDir dir(PagePath);
    if(!dir.mkpath(dir.absolutePath())) {
        qDebug() << "error on path " << PagePath;
    }

    for(auto it = mAnswers.begin(); it != mAnswers.end(); ++it) {
        ((Answer * )(*it))->Save(PagePath);
    }

    QFile file(QString("%1/text.txt").arg(PagePath));

    if (file.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) {
        QTextStream stream( &file );
        stream << mText;
    } else {
        qDebug() << file.errorString() << file.fileName();
    }

    file.close();

    Globals::Records.push_back(QString("%1/text.txt").arg(PagePath));

    if(!Init(QString("%1/info.xml").arg(PagePath))) {
        return;
    }

    QDomElement info = doc.createElement("info");
    AddChildValue(info, "visible", (mHeaderVisible) ? "1" : "0");
    AddImage(info, mTopImage, "head", PagePath);
    AddImage(info, mBodyImage, "body", PagePath);
    AddImage(info, mBottomImage, "bottom", PagePath);

    Finish(info);
}

void Page::setTopImage(QString path) {
#ifdef Q_OS_WIN
    mTopImage = path.right(path.size() - 8);
#else
    mTopImage = path.right(path.size() - 7);
#endif
    topImageChanged();
}

void Page::setBodyImage(QString path){
#ifdef Q_OS_WIN
    mBodyImage  = path.right(path.size() - 8);
#else
    mBodyImage  = path.right(path.size() - 7);
#endif

    bodyImageChanged();
}

void Page::setBottomImage(QString path) {
#ifdef Q_OS_WIN
    mBottomImage  = path.right(path.size() - 8);
#else
    mBottomImage  = path.right(path.size() - 7);
#endif
    bottomImageChanged();
}

void Page::Load(QString path) {
    QString PagePath = QString("%1/%2").arg(path).arg(mHead);

    int index = Globals::Objects[mHead].indexOf("text.txt");
    if(index != -1) {
        QFile file(QString("%1/text.txt").arg(PagePath));

        if (file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QTextStream stream( &file );
            mText = stream.readAll();
        } else {
            qDebug() << file.errorString() << file.fileName();
        }

        Globals::Objects[mHead].removeAt(index);
        file.close();
    }

    index = Globals::Objects[mHead].indexOf("info.xml");
    if(index != -1) {
        InitReading(PagePath + "/info.xml");
        QDomElement info = doc.documentElement();

        mHeaderVisible = info.firstChildElement("visible").text().toInt();
        QString image = info.firstChildElement("head_image").text();

        if(image.size()) {
            mTopImage = PagePath + '/' + image;
            index = Globals::Objects[mHead].indexOf(image);
            Globals::Objects[mHead].removeAt(index);
        }

        image = info.firstChildElement("body_image").text();
        if(image.size()) {
            mBodyImage = PagePath + '/' + image;
            index = Globals::Objects[mHead].indexOf(image);
            Globals::Objects[mHead].removeAt(index);
        }

        image = info.firstChildElement("bottom_image").text();
        if(image.size()) {
            mBottomImage = PagePath + '/' + image;
            index = Globals::Objects[mHead].indexOf(image);
            Globals::Objects[mHead].removeAt(index);
        }

        index = Globals::Objects[mHead].indexOf("info.xml");
        Globals::Objects[mHead].removeAt(index);
    }
    for(auto it = Globals::Objects[mHead].begin();
        it != Globals::Objects[mHead].end(); ++it) {

        Answer * ptr = new Answer(*it);
        ptr->Load(PagePath + "/answers");

        mAnswers.append(ptr);
    }


}
