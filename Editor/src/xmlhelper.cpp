#include "xmlhelper.h"
#include "globals.h"

#include <QFileInfo>
#include <QDebug>

xmlHelper::xmlHelper() : doc(), xmlFile() {

}

bool xmlHelper::Init(QString Path) {
    xmlFile.setFileName(Path);
    if (!xmlFile.open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)) {
        qDebug() << xmlFile.errorString() << xmlFile.fileName();
        return false;
    }

    Globals::Records.push_back(Path);

    doc.clear();

    return true;
}

void xmlHelper::AddChildValue(QDomElement &parent, QString Name, QString Value) {
    QDomElement childElement = doc.createElement(Name);
    QDomText childText = doc.createTextNode(Value);
    childElement.appendChild(childText);

    parent.appendChild(childElement);
}

void xmlHelper::AddImage(QDomElement &parent, QString image, QString name, QString Path) {
    if(!image.size()) {
        return;
    }

    QFileInfo info(image);

    if(!info.exists()) {
        qDebug() << "image Not Existed";
        return;
    }

    QString l_name = QString("%1_image").arg(name);
    QString filename = QString("%1.%2").arg(l_name).arg(info.suffix());
    QString rezPath = QString("%1/%2").arg(Path).arg(filename);
    if(QFile::copy(info.absoluteFilePath(), rezPath)) {
        qDebug() << "success";
        Globals::Records.push_back(rezPath);
        AddChildValue(parent, l_name, filename);
    }
}

void xmlHelper::Finish(QDomElement &child) {
    doc.appendChild(child);

    QTextStream out(&xmlFile);
    out << doc.toString();
    xmlFile.close();
}

void xmlHelper::InitReading(QString path) {
        xmlFile.setFileName(path);
        if (!xmlFile.open(QIODevice::ReadOnly)) {
            qDebug() << xmlFile.errorString() << xmlFile.fileName();
            return;
        }

        QString err;
        if(!doc.setContent(&xmlFile, &err)) {
            qDebug() << err << xmlFile.fileName();
            return;
        }

        xmlFile.close();
}

