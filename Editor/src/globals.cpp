#include "globals.h"
#include <QFile>
#include <QDateTime>
#include <QTextStream>

QVector < QString > Globals::Records;
QMap < QString, QVector < QString > > Globals::Objects;
QFile* Globals::log_file = new QFile("./" + QDate::currentDate().toString() + ".log");


Globals::Globals() {}

void Globals::installQtMessageHandler() {

    if(log_file->open(QIODevice::Append | QIODevice::WriteOnly)) {
        qInstallMessageHandler([](QtMsgType type, const QMessageLogContext &context, const QString &msg) {
            (void)context;
            QTextStream stream(log_file);

            stream << '[' << QTime::currentTime().toString("hh:mm:ss") << "]  " << msg << '\n';

            if(type == QtFatalMsg) {
                abort();
            }
        });
    }

}
