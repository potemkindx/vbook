#include "book.h"
#include "page.h"
#include "globals.h"
#include "inventory.h"

#include <QQmlEngine>
#include <QtQml>
#include <QDebug>
#include <QDir>
#include <QFileInfo>

Book::Book(QObject *parent) : QObject(parent), mPages(), mCurrentPage(NULL),  mInventories(), mCurrentInventory(NULL), mPath(),mIsInit(false) {}

void Book::registerType() {
    qmlRegisterType<Book, 1>("Book", 1, 0, "Book");
}

void Book::AddPage(QString text) {
    if (text.toUpper() == "INVENTORY") {
        return;
    }

    for(auto it = mPages.begin(); it != mPages.end(); ++it) {
        if(((Page * )(*it))->mHead.toUpper() == text.toUpper()) {
            qDebug() << "the same page";
            return;
        }
    }

    mPages.append(new Page(text));
    pagesChanged();
}

void Book::SelectPage(int index) {
    if(index < 0 || index > mPages.size() || mPages.empty()) {
        return;
    }
    mCurrentPage = mPages.at(index);

    currentPageChanged();
}

void Book::RemovePage(int index) {
    if(index < 0 || index > mPages.size() || mPages.empty()) {
        return;
    }
    Page* page = (Page*)mPages.at(index);
    mPages.removeAt(index);
    page->deleteLater();
    pagesChanged();
}

QList<QObject *> Book::pages() {
    return mPages;
}

QObject *Book::currentPage() {
    return mCurrentPage;
}

void Book::AddInventory(QString text)
{
    mInventories.append(new Inventory(text));
    inventoriesChanged();
}

void Book::SelectInventory(int index)
{
    if(index < 0 || index > mInventories.size() || mInventories.empty()) {
        return;
    }
    mCurrentInventory = mInventories.at(index);

    currentInventoryChanged();
}

void Book::RemoveInventory(int index) {
    if(index < 0 || index > mInventories.size() || mInventories.empty()) {
        return;
    }
    Inventory * inv = (Inventory *)mInventories.at(index);
    mInventories.removeAt(index);
    inv->deleteLater();
    inventoriesChanged();
}

QList<QObject *> Book::inventories() {
    return mInventories;
}

QObject *Book::currentInventory() {
    return mCurrentInventory;
}

void Book::init(QString path) {
#ifdef Q_OS_WIN
    path.replace(0, 8, "");
#else
    path.replace(0, 7, "");
#endif

    mPath = path;

    Page * globals = new Page("globals");
    mPages.append(globals);

    Page * init = new Page("start");
    mPages.append(init);
    pagesChanged();

    mIsInit = true;
    isInitChanged();
}

void Book::load(QString path) {
#ifdef Q_OS_WIN
    path.replace(0, 8, "");
#else
    path.replace(0, 7, "");
#endif
    //mPath = path;
    QFileInfo fileinf(path);
    mPath = fileinf.path();

    InitReading(path);

    QDomElement RCC = doc.documentElement();
    QDomElement qresource = RCC.firstChildElement("qresource");
    if(qresource.isNull()) {
        qDebug() << "qResource is NULL";
    }
    QDomElement file = qresource.firstChildElement("file");
    Globals::Objects.clear();

    while(!file.isNull()) {
        QFileInfo info(mPath + "/" + file.text());

        if(info.exists()) {
            QDir parent = info.dir();
            if(parent.dirName().toUpper() == "ANSWERS") {
                parent.cdUp();
            }
            Globals::Objects[parent.dirName()].push_back(info.fileName());
        }

        file = file.nextSiblingElement("file");
    }

    for(auto it = mPages.begin(); it != mPages.end(); ++it) {
        delete *it;
    }

    mPages.clear();
    mInventories.clear();

    auto it = Globals::Objects.begin();
    for(; it != Globals::Objects.end(); ++it) {
        if(it.key().toUpper() != "INVENTORY") {
            Page *ptr = new Page(it.key());
            ptr->Load(mPath + + "/Answers/");
            mPages.append(ptr);
        } else {
            for(auto iit = it.value().begin(); iit != it.value().end(); ++iit) {
                Inventory *ptr = new Inventory(*iit);
                ptr->Load(mPath + "/Inventory/");
                mInventories.push_back(ptr);
            }
        }

    }

    pagesChanged();
    inventoriesChanged();

    mIsInit = true;
    isInitChanged();
}

void Book::save() {
    if(!mPath.size()) {
        return;
    }

    if(!Init(QString("%1/book.qrc").arg(mPath))) {
        return;
    }

    Globals::Records.clear();

    QString AnsPath = mPath + "/Answers";
    for(auto it = mPages.begin(); it != mPages.end(); ++it) {
        ((Page * )(*it))->Save(AnsPath);
    }

    QString InvPath = mPath + "/Inventory";

    QDir dir(InvPath);
    if(!dir.mkpath(dir.absolutePath())) {
        qDebug() << "error on path " << dir.absolutePath();
        return;
    }

    for(auto it = mInventories.begin(); it != mInventories.end(); ++it) {
        ((Inventory *)(*it))->Save(InvPath);
    }
    QDomElement RCC = doc.createElement("RCC");
    QDomElement qresource = doc.createElement("qresource");
    qresource.setAttribute("prefix", "/");

    for(auto it = Globals::Records.begin(); it != Globals::Records.end(); ++it) {
        AddChildValue(qresource, "file", it->right(it->size() - mPath.size() -1));
    }
    RCC.appendChild(qresource);
    Finish(RCC);
}

