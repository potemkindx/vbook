#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QFile>
#include <QTextStream>
#include <QDateTime>

#include "book.h"
#include "globals.h"
#include "documenthandler.h"

int main(int argc, char *argv[]) {
    //Globals::installQtMessageHandler();

    QGuiApplication app(argc, argv);

    app.addLibraryPath("./(void)context;/Libs");
    app.addLibraryPath("./Qt/QML");

    Book::registerType();
    qmlRegisterType<DocumentHandler>("DocHandler", 1, 0, "DocumentHandler");

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));

    return app.exec();
}

