TEMPLATE = app

QT += qml quick xml
CONFIG += c++11

HEADERS += \
    include/book.h \
    include/page.h \
    include/answer.h \
    include/action.h \
    include/globals.h \
    include/inventory.h \
    include/item.h \
    include/xmlhelper.h \
    include/documenthandler.h


SOURCES += src/main.cpp \
    src/book.cpp \
    src/page.cpp \
    src/answer.cpp \
    src/action.cpp \
    src/globals.cpp \
    src/inventory.cpp \
    src/item.cpp \
    src/xmlhelper.cpp \
    src/documenthandler.cpp

INCLUDEPATH += include

DEPENDPATH += src

RESOURCES += qml/qml.qrc

include(deployment.pri)
